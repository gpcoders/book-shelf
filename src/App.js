import React from 'react'
import { Route } from 'react-router-dom'

import './App.css'
import ListBook from "./components/ListBook";
import AddNewBook from "./components/AddNewBook";

class BooksApp extends React.Component {
    render() {
        return (
            <div className="app">
                <Route path="/add-book" component={AddNewBook}/>
                <Route path="/" exact component={ListBook}/>
            </div>
        )
    }
}

export default BooksApp
