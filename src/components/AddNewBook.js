import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { BarLoader } from 'react-spinners';
import { DebounceInput } from 'react-debounce-input';
import * as bookApi from '../BooksAPI';

class AddNewBook extends Component {

    /**
     * @Description: Init state
     * @type {{books: Array}}
     */
    state = {
        spinnerColor: "#2e7c31",
        spinnerWidth: 500,
        books: [],
        searchState: false,
        loading: false
    }

    /**
     * @Description: Invoke the lifecycle hook after component is loaded
     */
    searchBooks(e) {


        /**
         * @Description: Returning if field value is null
         */
        if (e.event === "") {
            return false
        }

        /**
         * @Description: setting spinner true
         */
        this.setState({
            loading: true
        });

        /**
         * @Description: Getting all books and passing them to state
         */
        bookApi.search(e.event, 20).then(response => {
            console.log(response)
            /**
             * @Description: Updating the state with server response
             */
            if ( response.length > 0 ) {
                this.setState({
                    books: response,
                    searchState: true,
                    loading: false
                });
                console.log(response)
            } else {
                this.setState({
                    loading: false,
                    books: []
                })
            }

        })
    }

    /**
     * @Description: Changing the status
     */
    changeBookStatus(e) {

        /**
         * @Description: Updating the books state
         */
        this.setState({
            books: this.state.books.filter((book) => book.id !== e.id),
            loading: true
        })

        /**
         * @Description: Updating data on server
         */
        bookApi.update({ id: e.id },  e.event )
            .then(response => {
                console.log(response)
                this.updateState()
            })
    }

    /**
     *
     * @returns {*}
     */
    updateState = () => {

        /**
         * @Description: setting spinner true
         */
        this.setState({
            loading: true
        })

        /**
         * @Description: Getting all books and passing them to state
         */
        bookApi.getAll().then(response => {
            /**
             * @Description: Updating the state with server response
             */
            this.setState({
                books: response,
                loading: false
            });
            console.log(response)
        })
    }

    render() {

        const { books, loading, spinnerColor, spinnerWidth } = this.state


        return (
            <div>
                <div className="search-books">
                    <div className="search-books-bar">
                        <Link to="/" className="close-search">Close</Link>
                        <div className="search-books-input-wrapper">

                            <DebounceInput
                                minLength={1}
                                debounceTimeout={500}
                                className="form-control"
                                onChange={event => this.searchBooks({event: event.target.value})}
                                placeholder="Type Here To Search By Book Name" name="keywords"
                                type="text"
                            />

                        </div>
                    </div>
                    <div className="search-books-results">
                        <ol className="books-grid"></ol>
                    </div>
                </div>
                <div className="list-books">

                    <br />
                    <div className="ldrs">
                        <BarLoader
                            color={spinnerColor}
                            loading={loading}
                            width={spinnerWidth}
                        />
                    </div>

                    <div className="list-books-content">
                        <div>
                            <div className="bookshelf">
                                <h2 className="bookshelf-title">Search Books :</h2>

                                <div className="bookshelf-books">
                                    <ol className="books-grid">
                                        {books.map((book, index) => (
                                            <li key={index}>
                                                <div className="book">
                                                    <div className="book-top">

                                                        <div className="book-cover" style={{ width: 128, height: 193, backgroundImage: `url(${book.imageLinks.thumbnail})` }}></div>
                                                        <div className="book-shelf-changer">
                                                            <select value={ book.readingModes.text === true ? "none": "" }  onChange={event => this.changeBookStatus({event: event.target.value, id: book.id})}>
                                                                <option value="none" disabled>Move to...</option>
                                                                <option value="currentlyReading">Currently Reading</option>
                                                                <option value="wantToRead">Want to Read</option>
                                                                <option value="read">Read</option>
                                                                <option value="none">None</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="book-title">{book.title}</div>
                                                    <div className="book-authors">{book.author}</div>
                                                </div>
                                            </li>
                                        ))}
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="open-search">
                        <Link to="/add-book" >Add a book</Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddNewBook