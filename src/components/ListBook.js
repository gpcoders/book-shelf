import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { BarLoader } from 'react-spinners';
import CurrentlyReading from "./sub-components/CurrentlyReading";
import Read from "./sub-components/Read";
import WantToRead from "./sub-components/WantToRead";

import * as bookApi from "../BooksAPI";

class ListBook extends Component {


    /**
     * @Description: Init state
     */
    state = {
        spinnerColor: "#2e7c31",
        spinnerWidth: 500,
        loading: false,
        books: []
    }

    /**
     * @Description: Invoke the lifecycle hook after component is loaded
     */
    componentDidMount() {


        /**
         * @Description: setting spinner true
         */
        this.setState({
            loading: true
        })

        /**
         * @Description: Getting all books and passing them to state
         */
        bookApi.getAll().then(response => {
            /**
             * @Description: Updating the state with server response
             */
            this.setState({
                books: response,
                loading: false
            });

            console.log(response)
        })
    }

    /**
     * @Description: Changing the status
     */
    changeBookStatus(e) {

        /**
         * @Description: Updating the books state
         */
        this.setState({
            books: this.state.books.filter((book) => book.id !== e.id),
            loading: true
        })

        /**
         * @Description: Updating data on server
         */
        bookApi.update({ id: e.id },  e.event )
            .then(response => {
                this.updateState()
            })
    }

    /**
     *
     * @returns {*}
     */
    updateState = () => {

        /**
         * @Description: setting spinner true
         */
        this.setState({
            loading: true
        })

        /**
         * @Description: Getting all books and passing them to state
         */
        bookApi.getAll().then(response => {
            /**
             * @Description: Updating the state with server response
             */
            this.setState({
                books: response,
                loading: false
            });
        })
    }


    render() {
        const { books, loading, spinnerColor, spinnerWidth } = this.state
        return (
            <div className="list-books">
                <div className="list-books-title">
                    <h1>Book Shelf</h1>
                </div>

                <br />
                <div className="ldrs">
                    <BarLoader
                        color={spinnerColor}
                        loading={loading}
                        width={spinnerWidth}
                    />
                </div>

                <div className="list-books-content">
                    <div>
                        <div className="bookshelf">
                            <h2 className="bookshelf-title">Currently Reading</h2>

                            <div className="bookshelf-books">
                                <CurrentlyReading
                                      books={books}
                                      loading={loading}
                                      spinnerColor={spinnerColor}
                                      spinnerWidth={spinnerWidth}
                                      onChangeBookStatus={(book) => {
                                        this.changeBookStatus(book)
                                }}/>
                            </div>
                        </div>
                        <div className="bookshelf">
                            <h2 className="bookshelf-title">Want to Read</h2>
                            <div className="bookshelf-books">
                                <WantToRead books={books}
                                            loading={loading}
                                            spinnerColor={spinnerColor}
                                            spinnerWidth={spinnerWidth}
                                            onChangeBookStatus={(book) => {
                                                this.changeBookStatus(book)
                                            }}/>
                            </div>
                        </div>
                        <div className="bookshelf">
                            <h2 className="bookshelf-title">Read</h2>
                            <div className="bookshelf-books">
                                <Read books={books}
                                      loading={loading}
                                      spinnerColor={spinnerColor}
                                      spinnerWidth={spinnerWidth}
                                      onChangeBookStatus={(book) => {
                                          this.changeBookStatus(book)
                                      }}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="open-search">
                    <Link to="/add-book" >Add a book</Link>
                </div>
            </div>
        )
    }
}

export default ListBook;