import React, { Component } from 'react'

class CurrentlyReading extends Component {

    render() {
        const { books, onChangeBookStatus } = this.props

        return (
            <div>
                <ol className="books-grid">
                    {books.filter((book) => book.shelf === 'currentlyReading').map((book, index) => (
                        <li key={index}>
                            <div className="book">
                                <div className="book-top">

                                    <div className="book-cover"
                                         style={{ width: 128, height: 193, backgroundImage: `url(${(book.imageLinks.thumbnail) ? book.imageLinks.thumbnail : ""})` }}>
                                    </div>
                                    <div className="book-shelf-changer">
                                        <select value={book.shelf}  onChange={event => onChangeBookStatus({event: event.target.value, id: book.id})}>
                                            <option value="none" disabled>Move to...</option>
                                            <option value="currentlyReading">Currently Reading</option>
                                            <option value="wantToRead">Want to Read</option>
                                            <option value="read">Read</option>
                                            <option value="none">None</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="book-title">{book.title}</div>
                                <div className="book-authors">{book.author}</div>
                            </div>
                        </li>
                    ))}
                </ol>

            </div>

        )
    }
}

export default CurrentlyReading